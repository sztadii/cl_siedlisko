<?php

    return [
        'name'          =>  $core->lang['googlemap']['module_name'],
        'description'   =>  $core->lang['googlemap']['module_desc'],
        'author'        =>  'KrystianStd',
        'version'       =>  '1.0',
        'icon'          =>  'map',

        'install'       =>  function() use($core)
        {
            $core->db()->pdo()->exec("CREATE TABLE IF NOT EXISTS `googlemap` (
                `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
                `field` text NOT NULL,
                `value` text NOT NULL
            )");

            $core->db()->pdo()->exec("INSERT INTO `googlemap` (`field`, `value`) VALUES ('lat', '50.000')");
            $core->db()->pdo()->exec("INSERT INTO `googlemap` (`field`, `value`) VALUES ('lng', '10.000')");
            $core->db()->pdo()->exec("INSERT INTO `googlemap` (`field`, `value`) VALUES ('zoom', '10')");
            $core->db()->pdo()->exec("INSERT INTO `googlemap` (`field`, `value`) VALUES ('styles', '[]')");
            $core->db()->pdo()->exec("INSERT INTO `googlemap` (`field`, `value`) VALUES ('custom', 'yes')");
        },

        'uninstall'     =>  function() use($core)
        {
            $core->db()->pdo()->exec("DROP TABLE `googlemap`");
        }
    ];
