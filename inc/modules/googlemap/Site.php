<?php

    namespace Inc\Modules\GoogleMap;

    class Site
    {
        public $core;

        public function __construct($object)
        {
            $this->core = $object;
            $this->core->tpl->set('googlemap',$this->showGoogleMap());
        }

        private function showGoogleMap()
        {
            $this->getMap();
            return $this->core->tpl->draw(MODULES.'/googlemap/view/template.html');
        }

        private function getMap()
        {
            //Get module parameters
            $parameters = $this->core->db('googlemap')->toArray();

            //Create parameters vars    
            foreach($parameters as $row)
                $parameters[$row['field']] = $row['value'];

            $this->core->tpl->set('googlemap',$parameters);

            if($parameters['custom'] == 'yes') {
                $this->core->addCSS(url(MODULES.'/googlemap/view/template.css'), 'header');
                $this->core->addJS(url(MODULES.'/googlemap/view/template.js'), 'footer');
            }
        }
    }