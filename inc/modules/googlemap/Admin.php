<?php

    namespace Inc\Modules\GoogleMap;

    class Admin
    {
        public $core;

        public function __construct($object)
        {
            $this->core = $object;
        }

        public function navigation()
        {
            return [
                $this->core->lang['general']['settings'] => 'general',
            ];
        }

        public function general()
        {
            $settings = $this->core->db('googlemap')->toArray();

            //Create Module vars
            foreach($settings as $row)
                $settings[$row['field']] = $row['value'];

            $this->core->tpl->set('googlemap', $settings);
            return $this->core->tpl->draw(MODULES.'/googlemap/view/admin/general.html');
        }

        public function saveGeneral()
        {
            unset($_POST['save']);
            if(checkEmptyFields(array_keys($_POST), $_POST))
            {
                $this->core->setNotify('failure', $this->core->lang['general']['empty_inputs']);
                redirect(url([ADMIN, 'googlemap', 'general']), $_POST);
            }
            else
            {
                $errors = 0;
                foreach($_POST as $field => $value)
                {
                    if(!$this->core->db('googlemap')->where('field', $field)->save(['value' => $value]))
                    $errors++;
                }

                if(!$errors)
                    $this->core->setNotify('success', $this->core->lang['settings']['save_settings_success']);
                else
                    $this->core->setNotify('failure', $this->core->lang['settings']['save_settings_failure']);

                redirect(url([ADMIN, 'googlemap', 'general']));
            }
        }
    }