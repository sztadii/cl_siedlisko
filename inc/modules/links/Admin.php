<?php

    namespace Inc\Modules\Links;

    class Admin
    {

        public $core;

        public function __construct($object)
        {
            $this->core = $object;
		}

        public function navigation()
        {
            return [
                $this->core->lang['general']['manage']	=> 'manage',
                $this->core->lang['links']['add']	=> 'add',
            ];
        }

        /**
        * list of links
        */
        public function manage()
        {
            $rows = $this->core->db('links')->toArray();
        	if(count($rows))
        	{
	        	foreach($rows as &$row)
	        	{
                    $row['tag'] = $this->core->tpl->noParse('{$link.'.$row['slug'].'}');
	        		$row['editURL'] = url([ADMIN, 'links', 'edit', $row['id']]);
		        	$row['delURL'] = url([ADMIN, 'links', 'delete', $row['id']]);
	        	}
        	}

        	$this->core->tpl->set('links', $rows);
        	return $this->core->tpl->draw(MODULES.'/links/view/admin/manage.html');
		}

        /**
        * add new link
        */
        public function add()
        {
            if(!empty($redirectData = getRedirectData()))
            {
                $assign = $redirectData;
                $assign['content'] = $this->core->tpl->noParse($assign['content']);
            }
            else
                $assign = ['name' => '', 'content' => ''];

            $assign['title'] = $this->core->lang['links']['add'];
            
        	$this->core->tpl->set('links', $assign);
        	return $this->core->tpl->draw(MODULES.'/links/view/admin/form.html');
		}

        /**
        * edit link
        */
        public function edit($id)
        {
            if(!empty($row = $this->core->db('links')->oneArray($id)))
    	    {                
                if(!empty($redirectData = getRedirectData()))
                {
                    $assign['name'] = $redirectData['name'];
                    $assign['content'] = $redirectData['content'];
                }
                $assign = htmlspecialchars_array($row);       
                $assign['content'] = $this->core->tpl->noParse($assign['content']);
            }
            else
                redirect(url([ADMIN, 'links', 'manage']));

            $assign['title'] = $this->core->lang['links']['edit'];
         	$this->core->tpl->set('links', $assign);
        	return $this->core->tpl->draw(MODULES.'/links/view/admin/form.html');
        }

        /**
        * remove link
        */
        public function delete($id)
        {
			if($this->core->db('links')->delete($id))
                $this->core->setNotify('success', $this->core->lang['links']['delete_success']);
			else
				$this->core->setNotify('failure', $this->core->lang['links']['delete_failure']);

			redirect(url([ADMIN, 'links', 'manage']));
        }

        /**
        * save link
        */
        public function save($id = null)
        {
            unset($_POST['save']);

        	if(checkEmptyFields(['name', 'content'], $_POST))
    		{
    			$this->core->setNotify('failure', $this->core->lang['general']['empty_inputs']);

                if(!$id)
                    redirect(url([ADMIN, 'links', 'add']));
                else
                    redirect(url([ADMIN, 'links', 'edit', $id]));
    		}

            $_POST['name'] = trim($_POST['name']);
            $_POST['slug'] = createSlug($_POST['name']);

            if(!$id) // new
            {
                $location = url([ADMIN, 'links', 'add']);
                if(!$this->core->db('links')->where('slug', $_POST['slug'])->count())
                {
                    if($this->core->db('links')->save($_POST))
                    {
                        $location = url([ADMIN, 'links', 'edit', $this->core->db()->lastInsertId()]);
                        $this->core->setNotify('success', $this->core->lang['links']['save_success']);
                    }
                    else
                        $this->core->setNotify('failure', $this->core->lang['links']['save_failure']);
                }
                else
                    $this->core->setNotify('failure', $this->core->lang['links']['already_exists']);
            }
            else    // edit
            {
                if(!$this->core->db('links')->where('slug', $_POST['slug'])->where('id', '<>', $id)->count())
                {
                    if($this->core->db('links')->where($id)->save($_POST))
                        $this->core->setNotify('success', $this->core->lang['links']['save_success']);
                    else
                        $this->core->setNotify('failure', $this->core->lang['links']['save_failure']);
                }
                else
                    $this->core->setNotify('failure', $this->core->lang['links']['already_exists']);

                $location =  url([ADMIN, 'links', 'manage', $id]);
            }

            redirect($location, $_POST);
        }
    }