<?php

    return [
        'name'          =>  $core->lang['links']['module_name'],
        'description'   =>  $core->lang['links']['module_desc'],
        'author'        =>  'Krystianstd',
        'version'       =>  '1.0',
        'icon'          =>  'link',

        'install'       =>  function() use($core)
        {
            $core->db()->pdo()->exec("CREATE TABLE IF NOT EXISTS `links` (
                `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
                `name` text NOT NULL,
                `slug` text NOT NULL,
                `content` text NOT NULL
                )");
        },
        'uninstall'     =>  function() use($core)
        {
            $core->db()->pdo()->exec("DROP TABLE `links`");
        }
    ];