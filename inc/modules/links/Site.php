<?php

    namespace Inc\Modules\Links;

    class Site
    {

        public $core;

        public function __construct($object)
        {
            $this->core = $object;
            $this->_importLinks();
		}

        private function _importLinks()
        {
            $rows = $this->core->db('links')->toArray();

            $links = [];
            foreach($rows as $row)
            {
                $links[$row['slug']] = $row['content'];
            }

            return $this->core->tpl->set('link', $links);
        }
    }