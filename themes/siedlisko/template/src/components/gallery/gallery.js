var gallery;

gallery = new function () {

  //catch DOM
  var $el;
  var $box;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".gallery");
    $box = $el.find(".gallery__box");

    if ($box.length) {
      $box.lightGallery({
        mode: 'lg-slide'
      });
    }
  };
};